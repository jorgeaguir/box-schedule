// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import * as firebase from 'firebase'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App/>',
    created () {
        firebase.initializeApp({
            apiKey: 'AIzaSyB5QMNzbml1G3doz0u32qJaBuxbi6LQ4z0',
            authDomain: 'box-manager-31954.firebaseapp.com',
            databaseURL: 'https://box-manager-31954.firebaseio.com',
            projectId: 'box-manager-31954',
            storageBucket: 'box-manager-31954.appspot.com',
        })
    }
})
